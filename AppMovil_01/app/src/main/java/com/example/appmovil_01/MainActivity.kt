package com.example.appmovil_01

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.tooling.preview.Preview

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            //SumApp()
            MostrarValores();
        }
    }
}

fun sumaValores(a: Float, b: Float): Float {
    return a + b
}

@Composable
fun MostrarValores(){
    // Declarar variables con remember
    // remember permite que la variable conserve su valor entre recomposiciones de la interfaz de usuario
    var numero1 by remember {
        mutableStateOf("");
    }

    var numero2 by remember {
        mutableStateOf("");
    }

    var resultado by remember {
        mutableStateOf<String?>(null)
    }

    Column (
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ){
        TextField(
            value = numero1,
            onValueChange = {numero1 = it},
            label = { Text(text = "Numero 1")});

        Spacer(modifier = Modifier.height(8.dp));

        TextField(value = numero2,
            onValueChange = {numero2 = it },
            label = { Text(text = "Numero 2")});

        Spacer(modifier = Modifier.height(16.dp))

        Button(onClick = {
            resultado = ProcesaNumeros(numero1, numero2);
        }){
            Text(text = "Sumar")
        }
        
        Spacer(modifier = Modifier.height(16.dp))

        resultado.let {
            Text(text = "Resultado: $it")
        }
    }
}

fun ProcesaNumeros(a: String, b: String): String{
    val numero1 = a.toFloatOrNull();
    val numero2 = b.toFloatOrNull();

    val strResultado = if (numero1 != null && numero2 != null) {
        sumaValores(numero1, numero2).toString();
    } else {
        "Entrada no valida";
    }

    return strResultado;
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MostrarValores();
}
